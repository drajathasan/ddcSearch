<?php
/**
*
* Display Collection by Dewey Decimal Classification
* Author : Erwan Setyo Budi (erwans818@gmail.com)
* Heavy Modification for layout and paginition By Drajat Hasan (drajathasan20@gmail.com)
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
*
*/

// be sure that this file not accessed directly
if (!defined('INDEX_AUTH')) {
    die("can not access this file directly");
} elseif (INDEX_AUTH != 1) {
    die("can not access this file directly");
}
// Require template maker
require LIB.'paginitionJS/createDDCTemplate.inc.php';
?>
<!doctype html>
<html lang="en" class="no-js">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link href='https://fonts.googleapis.com/css?family=Jaldi:400,700' rel='stylesheet' type='text/css'>
  <link href="<?php echo $sysconf['template']['dir'].'/'.$sysconf['template']['theme']; ?>/css/font-awesome.css" rel="stylesheet"> 
  <link rel="stylesheet" href="<?php echo $sysconf['template']['dir'].'/'.$sysconf['template']['theme']; ?>/css/reset.css"> <!-- CSS reset -->
  <script type="text/javascript" src="<?php SWB;?>js/updater.js"></script>
  <script type="text/javascript" src="<?php SWB;?>js/gui.js"></script>
  <script type="text/javascript" src="<?php SWB;?>js/jquery.twbsPagination.js"></script>
  <title><?php echo $sysconf['library_name']; ?>  <?php echo $sysconf['library_subname']; ?></title>
</head>
<body>
  <h1>Collection View by Dewey Decimal Classification</h1>
  
  <?php
  // Create Object
  $ddcTemplate = new createDDCTemplate();
  // Set stylesheet
  $ddcTemplate->createStyle();
  // Loop
  for ($i=0; $i < 10; $i++) { 
    $ddcTemplate->createHead($i.'00');
    $ddcTemplate->createChild($i.'00');
  }
  // Setout
  echo $ddcTemplate->printOut();
  ?>
  

<script type="text/javascript">
    /* Acordion */
    $('.acordion').click(function(){
      var data = $(this).attr('data');
      var status = $(this).attr('status');
      var classi = $('span').attr('classi');
      if (status == 0) {
        $('.acordion').attr('status', 0);
        $(this).attr('status', 1);
        $('.acordion-child').html('');
        $('.acordion-child').slideUp();
        $('.contentClass').slideUp();
        $('.'+data).slideDown();
      } else {
        $(this).attr('status', 0);
        $('.'+data).slideUp();
        $('.contentClass').slideUp();
      }
    });

    /* Child button */
    $('span.button').click(function(){
      var status = $('.schild').attr('status');
      var classi = $(this).attr('classi');
      var schild = $('.'+classi+'Class-child')
      if (status == 0) {
        $('.schild').attr('status', 1);
        schild.slideDown();
        $('.'+classi+'schild-content').simbioAJAX('lib/paginitionJS/getData.php', {method: 'post', addData: 'class='+classi});
      } else {
        $('.schild-content').html('');
        $('.schild').slideUp();
        schild.slideDown();
        $('.'+classi+'schild-content').simbioAJAX('lib/paginitionJS/getData.php', {method: 'post', addData: 'class='+classi});
      }
    });
</script>
</body>
</html>