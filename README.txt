ddcSearch

Plugin sederhana pencarian data buku berdasarkan DDC.

Cara pasang :

1. Pindahkan file jquery.twbsPagination.js kedalam folder js/
2. Pindahkan file mainsubject.inc.php kedalam folder lib/contents/
3. Pindahkan folder paginitionJS ke dalam folder lib/
4. Tambahkan sub menu pada navigation dengan cara :
   - buka file nav.php pada slimsroot/template/default/partials
   - Tambahkan script ini pada baris ke -7
   - "<li><a href="index.php?p=mainsubject"><?php echo __('Browse by DDC'); ?></a></li>"
5. Buka SLiMS anda dan lihat pada menu navbar
6. Selamat mencoba.
