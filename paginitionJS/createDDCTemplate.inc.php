<?php
/**
* Dewey Decimal Classification template maker
* Copyright 2018 Drajat Hasan (drajathasan20@gmail.com)
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
*
*/ 
// Except Notice
error_reporting(E_ALL & ~E_NOTICE);

class createDDCTemplate
{
	public $element = array();

	public function createStyle()
	{
		$style  = '<style type="text/css">';
		$style .= '.acordion {';
		$style .= 'display: block;';
		$style .= 'width: 100%;';
		$style .= 'padding: 10px;';
		$style .= 'background: #3e3e3e;';
		$style .= 'color: white;';
		$style .= 'cursor: pointer;';
		$style .= '}';
		$style .= 'span {';
		$style .= 'width: 100%;';
		$style .= 'display: block;';
		$style .= 'padding: 10px;';
		$style .= 'cursor: pointer;';
		$style .= '}';
		$style .= '.schild-content  {';
		$style .= 'margin-left: 20px;';
		$style .= '}';
		$style .= '</style>';
		// setOut
		echo $style;
	}

	public function setLabel($class) 
	{
		$header = array(
				 '000' => 'Computer science, information & general works',
				 '100' => 'Philosophy & psychology',
				 '200' => 'Religion',
				 '300' => 'Social sciences',
				 '400' => 'Language',
				 '500' => 'Science',
				 '600' => 'Technology',
				 '700' => 'Arts & recreation',
				 '800' => 'Literature',
				 '900'=> 'History & geography'
			      );
		$label = array(
				 '000' => 'Computer science, knowledge & systems',
				 '010' => 'Bibliographies',
				 '020' => 'Library & information sciences',
				 '030' => 'Library & information sciences',
				 '040' => 'Unassigned',
				 '050' => 'Magazines, journals & serials',
				 '060' => 'Magazines, journals & serials',
				 '070' => 'News media, journalism & publishing',
				 '080' => 'Quotations',
				 '090' => 'Manuscripts & rare books',
				 '100' => 'Manuscripts & rare books',
				 '110' => 'Metaphysics',
				 '120' => 'Epistemology',
				 '130' => 'Parapsychology & occultism',
				 '140' => 'Philosophical schools of thought',
				 '150' => 'Psychology',
				 '160' => 'Philosophical logic',
				 '170' => 'Ethics',
				 '180' => 'Ancient, medieval, & Eastern philosophy',
				 '190' => 'Modern Western philosophy ',
				 '200' => 'Religion',
				 '210' => 'Philosophy & theory of religion',
				 '220' => 'The Bible',
				 '230' => 'Christianity',
				 '240' => 'Christian practice & observance',
				 '250' => 'Christian orders & local church',
				 '260' => 'Social & ecclesiastical theology',
				 '270' => 'History of Christianity',
				 '280' => 'Christian denominations',
				 '290' => 'Other religions',
				 '300' => 'Social sciences, sociology & anthropology',
				 '310' => 'Statistics',
				 '320' => 'Political science',
				 '330' => 'Economics',
				 '340' => 'Law',
				 '350' => 'Public administration & military science',
				 '360' => 'Social problems & social services',
				 '370' => 'Education',
				 '380' => 'Commerce, communications, & transportation',
				 '390' => 'Customs, etiquette, & folklore',
				 '400' => 'Language',
				 '410' => 'Linguistics',
				 '420' => 'English & Old English languages',
				 '430' => 'German & related languages',
				 '440' => 'French & related languages',
				 '450' => 'Italian, Romanian, & related languages',
				 '460' => 'Spanish, Portuguese, Galician',
				 '470' => 'Latin & Italic languages',
				 '480' => 'Classical & modern Greek languages',
				 '490' => 'Other languages',
				 '500' => 'Science',
				 '510' => 'Mathematics',
				 '520' => 'Astronomy',
				 '530' => 'Physics',
				 '540' => 'Chemistry',
				 '550' => 'Earth sciences & geology', 
				 '560' => 'Fossils & prehistoric life',
				 '570' => 'Biology',
				 '580' => 'Plants',
				 '590' => 'Animals (Zoology)',
				 '600' => 'Technology',
				 '610' => 'Medicine & health',
				 '620' => 'Engineering',
				 '630' => 'Agriculture',
				 '640' => 'Home & family management',
				 '650' => 'Management & public relations',
				 '660' => 'Chemical engineering',
				 '670' => 'Manufacturing',
				 '680' => 'Manufacture for specific uses',
				 '690' => 'Construction of buildings',
				 '700' => 'Arts',
				 '710' => 'Area planning & landscape architecture',
				 '720' => 'Architecture',
				 '730' => 'Sculpture, ceramics, & metalwork',
				 '740' => 'Graphic arts & decorative arts',
				 '750' => 'Painting',
				 '760' => 'Printmaking & prints',
				 '770' => 'Photography, computer art, film, video',
				 '780' => 'Music',
				 '790' => 'Sports, games & entertainment',
				 '800' => 'Literature, rhetoric & criticism',
				 '810' => 'American literature in English',
				 '820' => 'English & Old English literatures',
				 '830' => 'German & related literatures',
				 '840' => 'French & related literatures',
				 '850' => 'Italian, Romanian, & related literatures',
				 '860' => 'Spanish, Portuguese, Galician literatures',
				 '870' => 'Latin & Italic literatures',
				 '880' => 'Classical & modern Greek literatures',
				 '890' => 'Other literatures',
				 '900' => 'History',
				 '910' => 'Geography & travel',
				 '920' => 'Biography & genealogy',
				 '930' => 'History of ancient world (to ca. 499)',
				 '940' => 'History of Europe',
				 '950' => 'History of Asia',
				 '960' => 'History of Africa',
				 '970' => 'History of North America',
				 '980' => 'History of South America',
				 '990' => 'History of other areas'
			     );
	    $head = $header[$class];
	    $labe = $label[$class];
		$combine = array('head' => $head, 'label' => $labe);
		return $combine;
	}

	public function createHead($class)
	{
		$label = $this->setLabel($class);
		$this->element[] = '<div class="acordion" data="'.$class.'Class" status="0">';
		$this->element[] = '<span>'.$class.'&nbsp;'.$label['head'].'</span>';
		$this->element[] = '</div>';
	}

	public function createChild($class) 
	{
		$firstString = substr($class, 0,1);
		$this->element[] = '<div class="contentClass '.$class.'Class" style="display: none; margin-left: 20px;">';
		for ($x = 0 ; $x < 10; $x++){
			$label = $this->setLabel($firstString.$x.'0');
			$this->element[] = '<span class="button" classi="'.$firstString.$x.'0">'.$firstString.$x.'0 '.$label['label'].'</span>';
			$this->element[] = '<div class="animated infinate fadeIn schild '.$firstString.$x.'0Class-child" style="display: none;" status="0">';
			$this->element[] = '<div class="schild-content '.$firstString.$x.'0schild-content">';
			$this->element[] = '</div>';
			$this->element[] = '</div>';
		}
		$this->element[] = '</div>';
	}

	public function printOut()
	{
		$html_str = '';
		foreach ($this->element as $value) {
			$html_str .= $value;
		}
		return $html_str;
	}
}

?>