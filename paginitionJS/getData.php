<?php
/**
* PHP AJAX data displayer
* Copyright 2018 Drajat Hasan (drajathasan20@gmail.com)
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
*
*/ 
?>
<div class="infobox"></div>
<ul class="pagination-sm"></ul>
<?php
// Prevent Direct Access
if ($_SERVER['REQUEST_METHOD'] == 'GET') {
	die('direct access is\'t allow!');
}

// Key
define('INDEX_AUTH', '1');
require '../../sysconfig.inc.php';

// Process
if (isset($_POST['process']) AND isset($_POST['pageNum']) AND (isset($_POST['classification']))) {
	$page = (integer)$_POST['pageNum'];
	$page = $page - 1 ;
	$page = $page * 10;
	$classi = $dbs->escape_string($_POST['classification']);
	$current_book = "SELECT b.biblio_id, b.title, b.notes, b.image,
	          ba.biblio_id, ba.author_id, 
	          ma.author_id, ma.author_name, b.classification,b.publish_year, pu.publisher_name, pl.place_name FROM biblio AS b 
	          LEFT JOIN biblio_author AS ba ON ba.biblio_id = b.biblio_id 
	          LEFT JOIN mst_author AS ma ON ma.author_id = ba.author_id
	              LEFT JOIN mst_publisher AS pu ON pu.publisher_id = b.publisher_id
	              LEFT JOIN mst_place AS pl ON pl.place_id = b.publish_place_id
	          WHERE b.classification LIKE '$classi' LIMIT $page,10";
	$current_book_q = $dbs->query($current_book);
	while ($current_book_d = $current_book_q->fetch_assoc()) {
		$html_str  = '<div class="row" style="padding: 8px;">';
	 	$html_str .= '<div class="col-sm-10">';
	 	$html_str .= '<i class="fa fa-caret-right"></i>&nbsp;<a href="?p=show_detail&id='.$current_book_d['biblio_id'].'">'.$current_book_d['title'].' by '.$current_book_d['author_name'].'. Published by '.$current_book_d['publisher_name'].' at '.$current_book_d['place_name'].' on '.$current_book_d['publish_year'].'</a></li>';
	 	$html_str .= '</div>';
		$html_str .= '</div>';
		echo $html_str;
	} 
	exit();
} 

// Ajax Process
$class = (isset($_POST['class']))?substr($dbs->escape_string($_POST['class']), 0,2).'%':'00%';
$start = microtime(true);
$getNumrow = $dbs->query("SELECT b.biblio_id FROM biblio AS b WHERE b.classification LIKE '$class'");
$end = microtime(true);
$total = substr($end - $start, 0,5);
$num_rows = $getNumrow->num_rows;
$num_rows = ceil($num_rows/10);

if ($num_rows != 0 ) {
$visibility = ($num_rows == 1)?'display: none':NULL;
?>
<div class="contentPagi" style="width: 100%; padding: 10px;">
</div>
<ul class="pagination-sm"></ul>
<script type="text/javascript">
	$('.pagination-sm').attr('style', "<?php echo $visibility;?>");
	$('.pagination-sm').twbsPagination({
        totalPages: <?php echo $num_rows;?>,
        visiblePages: 7,
        onPageClick: function (event, page) {
            // $('#page-content').text('Page ' + page);
            $.post("<?php echo $_SERVER['PHP_SELF'];?>", {process:true,pageNum:page,classification:'<?php echo $class;?>'}, function(result){
            	$('.contentPagi').html(result);
            });
        }
    });
    $('.infobox').html('Ditemukan sebanyak <?php echo $getNumrow->num_rows;?> cantuman dalam <?php echo $total;?> detik');
</script>
<?php
} else {
	echo "<div style=\"background: #dc3545!important; width: 98%; color: white; padding: 20px;\">Maaf, klasifikasi ".$class." tidak tersedia dipangkalan data.</div>";
}
?>
